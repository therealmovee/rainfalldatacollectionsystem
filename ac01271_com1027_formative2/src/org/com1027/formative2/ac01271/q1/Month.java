/**
 * Month.java
 */

package org.com1027.formative2.ac01271.q1;

/**
 * Defines the enumerated values of each month
 * 
 * @author Alexis Chrysostomou
 */

public enum Month {
	JANUARY(0), FEBRUARY(1), MARCH(2), APRIL(3), MAY(4), JUNE(5), JULY(6), AUGUST(7), SEPTEMBER(8), OCTOBER(9),
	NOVEMBER(10), DECEMBER(11);

	/**
	 * Holds the identifier value of each month
	 */
	private final int monthId;

	/**
	 * @param monthId The identifier value of each month
	 */
	Month(int monthId) {
		this.monthId = monthId;
	}

	/**
	 * @return The enumerated value of each month
	 */
	public int getMonthId() {
		return this.monthId;
	}
}