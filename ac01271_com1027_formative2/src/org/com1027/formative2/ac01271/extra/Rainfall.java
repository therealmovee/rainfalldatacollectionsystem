/**
 * Rainfall.java
 */

package org.com1027.formative2.ac01271.extra;

/**
 * Defines the attributes & behaviors of the Rainfall object
 * 
 * @author Alexis Chrysostomou
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;

import org.com1027.formative2.ac01271.extra.Month;
import org.com1027.formative2.ac01271.extra.RainfallYear;

public class Rainfall {
	/**
	 * Array of RainfallYear objects used to store data about each year
	 */
	private RainfallYear[] rainfallYears = null;

	/**
	 * BufferedReader that will be used to read each line of a file
	 */
	private BufferedReader buffer = null;

	/**
	 * FileReader which will be used to read a file
	 */
	private FileReader inputFile = null;

	/**
	 * BufferedReader which will be used to count the number of lines of a file
	 */
	private BufferedReader countBuffer = null;

	/**
	 * FileReader which will be used to read a file in order to count its lines
	 */
	private FileReader countInputFile = null;

	/**
	 * Regular Expressions that defines the acceptable file format
	 */
	private static final String FILENAME_REGEX = "[a-zA-Z]*.txt";

	/**
	 * Constructor ( Creates an arrayList of RainfallYear objects based on the input
	 * parameter )
	 * 
	 * @param rainfallYear The array holding the rainfall data about each year
	 */
	public Rainfall(RainfallYear[] rainfallYears) {
		super();
		this.rainfallYears = rainfallYears;
	}

	/**
	 * Constructor
	 * 
	 * @param fileName The name of the file to be read in order to create a
	 *                 RainfallYear object
	 */
	public Rainfall(String fileName) {
		super();
		this.rainfallYears = readFile(fileName);
	}

	/**
	 * Private method that attempts to access and read a file and convert its data
	 * into an array of RainfallYear objects
	 * 
	 * @param fileName The name of the file which will be used to access it
	 * @return An array of RainfallYear objects holding yearly rainfall data
	 */
	private RainfallYear[] readFile(String fileName) {
		// Ensuring that the fileName has the correct format
		if (!fileName.matches(FILENAME_REGEX))
			throw new IllegalArgumentException("Invalid filename format.");

		try {
			this.inputFile = new FileReader(fileName);
			this.buffer = new BufferedReader(inputFile);

			String line = this.buffer.readLine();
			double[] rainfallMonths = new double[12];
			int yearIndex = 0;
			int year = 0;
			int previousYear = 0;

			// Get the total number of lines that the file contains
			int fileLines = calculateFileLines(fileName);

			RainfallYear[] yearlyData = new RainfallYear[fileLines];

			while (line != null) {
				// Store each word as an element of the array data
				String[] data = line.split(",");

				// Using a try method to ensure that year can be parsed as an Integer
				try {
					// Ensuring that all the years in the file are in sequential order
					if (previousYear + 1 != Integer.parseInt(data[0]) && year != 0)
						throw new IllegalArgumentException("Year order is not sequential in the file.");

					// Ensuring that year is not negative
					if (year < 0)
						throw new IllegalArgumentException("Invalid year.");

					year = Integer.parseInt(data[0]);
					previousYear = year;

				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

				// Collecting rainfallMonths data
				for (int i = 0; i < rainfallMonths.length; i++) {
					try {
						// Using a try method to ensure that rainfall month precipitation data can be
						// parsed as double variables
						rainfallMonths[i] = Double.parseDouble(data[i + 1]);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}

				yearlyData[yearIndex] = new RainfallYear(year, rainfallMonths);

				// Resetting the rainfallMonths array to store the new rainfall data of the next
				// line
				rainfallMonths = new double[12];
				yearIndex++;
				line = this.buffer.readLine();
			}

			return yearlyData;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (this.buffer != null) {
				try {
					// Closing the file
					this.buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	/**
	 * Private method that attempts to open and read its contents in order to get
	 * the number of lines it contains
	 * 
	 * @param fileName The name of the file used to access its contents
	 * @return The number of the total lines it contains
	 */
	private int calculateFileLines(String fileName) {
		int totalFileLines = 0;

		// Ensuring that the fileName has the correct format
		if (!fileName.matches(FILENAME_REGEX))
			throw new IllegalArgumentException("Invalid filename format.");

		try {
			this.countInputFile = new FileReader(fileName);
			this.countBuffer = new BufferedReader(countInputFile);

			String countLine = this.countBuffer.readLine();

			// While the buffer hasn't reached the end of the file
			while (countLine != null) {
				// Increment line counter
				++totalFileLines;

				// Read next line
				countLine = this.countBuffer.readLine();
			}

			return totalFileLines;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (countBuffer != null) {
				try {
					// Close the file
					this.countBuffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

	/**
	 * @return The highest average mean precipitation among all available years
	 */
	public double calculateHighestMeanAnnualRainfall() {
		double highestPrecipitation = Double.MIN_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() > highestPrecipitation) {
				highestPrecipitation = rainfallYear.calculateMeanRainfall();
			}
		}

		return highestPrecipitation;
	}

	/**
	 * @return The lowest average mean precipitation among all available years
	 */
	public double calculateLowestMeanAnnualRainfall() {
		double lowestPrecipitation = Double.MAX_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() < lowestPrecipitation) {
				lowestPrecipitation = rainfallYear.calculateMeanRainfall();
			}
		}

		return lowestPrecipitation;
	}

	/**
	 * 
	 * @param month The enumerated value of month used to get its mean precipitation
	 *              data
	 * @return The mean precipitation data of the given month for every available
	 *         year
	 */
	public double calculateMeanRainfallMonth(Month month) {
		double precipitation = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			precipitation += rainfallYear.getRainfallMonth(month);
		}

		return (precipitation / this.rainfallYears.length);
	}

	/**
	 * 
	 * @param year The year which will be used to get its average precipitation data
	 * @return The average precipitation data of the given year
	 */
	public double calculateMeanRainfallYear(int year) {
		double precipitation = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {

			if (rainfallYear.getYear() == year) {
				precipitation = rainfallYear.calculateMeanRainfall();
				break;
			}
		}

		return precipitation;
	}

	/**
	 * 
	 * @param year1 The first year holding the rainfall data
	 * @param year2 The second year holding the rainfall data
	 * @return The average mean precipitation between the two given years
	 */
	public double calculateMeanPrecipicationBetweenTwoYears(int year1, int year2) {
		double precipitation = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			// Getting the precipitation of year1
			if (rainfallYear.getYear() == year1) {
				precipitation += rainfallYear.calculateMeanRainfall();
			}

			// Getting the precipitation of year1
			if (rainfallYear.getYear() == year2) {
				precipitation += rainfallYear.calculateMeanRainfall();
			}
		}

		return (precipitation / 2F);
	}

	/**
	 * @return The lowest precipitation average of a specific month for all the
	 *         years
	 */
	public double calculateLowestMonthPrecipitation() {
		double[] monthPrecipitations = new double[12];
		double lowestPrecipitation = Double.MAX_VALUE;

		if (rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Looping through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			double[] yearMonthPrecipitations = rainfallYear.getRainfallMonths();

			// Adding all the month precipitation data of every year into an array
			for (int i = 0; i < yearMonthPrecipitations.length; i++) {
				monthPrecipitations[i] += yearMonthPrecipitations[i];
			}
		}

		// Dividing each month by all rainfallYears to get the precipitation for each
		// month of the year
		for (int i = 0; i < monthPrecipitations.length; i++) {
			monthPrecipitations[i] /= this.rainfallYears.length;
		}

		// Finding the lowest month precipitation
		for (int i = 0; i < monthPrecipitations.length; i++) {
			if (monthPrecipitations[i] < lowestPrecipitation) {
				lowestPrecipitation = monthPrecipitations[i];
			}
		}

		return lowestPrecipitation;
	}

	/**
	 * @return An integer array holding all the available years
	 */
	public int[] getYears() {
		int[] years = new int[this.rainfallYears.length];

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year Array is null.");

		// Loop through all the years
		for (int i = 0; i < this.rainfallYears.length; i++) {
			years[i] = this.rainfallYears[i].getYear();
		}

		return years;
	}

	/**
	 * Method that generates a Histogram based on all the precipitation data of all
	 * years
	 */
	public void generateHistogram() {
		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("railYears array is null");

		int[] years = this.getYears();
		double[] averagePrecipitations = new double[years.length];
		double maxPrecipitation = Double.MIN_VALUE;

		// Header
		System.out.println();
		System.out.println("Average Mean Precipitation Data Histogram");
		System.out.println("Years used: [" + years[0] + "-" + years[years.length - 1] + "]");
		System.out.println("Note: Each @@@@ represents 5.0 of the total average precipitation");

		// Collecting average precipitation data & storing them in an array
		for (int i = 0; i < this.rainfallYears.length; i++) {
			averagePrecipitations[i] = this.rainfallYears[i].calculateMeanRainfall();
		}

		// Finding the maximum height of the histogram
		for (int i = 0; i < averagePrecipitations.length; i++) {
			if (averagePrecipitations[i] > maxPrecipitation) {
				maxPrecipitation = averagePrecipitations[i];
			}
		}

		// Printing bars from 0 up to the average precipitation value
		for (double i = maxPrecipitation; i >= 1; i -= 5) {
			for (int k = 0; k < averagePrecipitations.length; k++) {
				if (averagePrecipitations[k] >= i) {
					System.out.print("@@@@     ");
				} else {
					System.out.print("         ");
				}
			}
			System.out.println();
		}

		System.out.println();

		DecimalFormat format = new DecimalFormat("00.0");

		// Printing the average precipitation value for each year
		for (int i = 0; i < averagePrecipitations.length; i++) {
			System.out.print("(" + format.format(averagePrecipitations[i]) + ")   ");
		}

		System.out.println();

		// Printing each year
		for (int i = 0; i < averagePrecipitations.length; i++) {
			System.out.print(years[i] + "     ");
		}

	}

}
