/**
 * Rainfall.java
 */

package org.com1027.formative2.ac01271.q2;

/**
 * Defines the attributes & behaviors of the Rainfall object
 * 
 * @author Alexis Chrysostomou
 */

public class Rainfall {
	/**
	 * Array of RainfallYear objects used to store data about each year
	 */
	private RainfallYear[] rainfallYears = null;

	/**
	 * Constructor ( Creates an arrayList of RainfallYear objects based on the input
	 * parameter )
	 * 
	 * @param rainfallYear The array holding the rainfall data about each year
	 */
	public Rainfall(RainfallYear[] rainfallYears) {
		super();

		// Ensuring that the rainfallYear array is not null
		if (rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array.");

		this.rainfallYears = rainfallYears;
	}

	/**
	 * @return The highest average mean precipitation among all available years
	 */
	public double calculateHighestMeanAnnualRainfall() {
		double highestPrecipication = Integer.MIN_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Loop through all years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() > highestPrecipication) {
				highestPrecipication = rainfallYear.calculateMeanRainfall();
			}
		}

		return highestPrecipication;
	}

	/**
	 * @return The lowest average mean precipitation among all available years
	 */
	public double calculateLowestMeanAnnualRainfall() {
		double lowestPrecipication = Integer.MAX_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Loop through all years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() < lowestPrecipication) {
				lowestPrecipication = rainfallYear.calculateMeanRainfall();
			}
		}

		return lowestPrecipication;
	}

	/**
	 * 
	 * @param month The enumerated value of month used to get its mean precipitation
	 *              data
	 * @return The mean precipitation data of the given month for every available
	 *         year
	 */
	public double calculateMeanRainfallMonth(Month month) {
		double precipication = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			precipication += rainfallYear.getRainfallMonth(month);
		}

		return (precipication / this.rainfallYears.length);
	}

	/**
	 * 
	 * @param year The year which will be used to get its average precipitation data
	 * @return The average precipitation data of the given year
	 */
	public double calculateMeanRainfallYear(int year) {
		double precipication = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.getYear() == year) {
				precipication = rainfallYear.calculateMeanRainfall();
				break;
			}
		}

		return precipication;
	}

	/**
	 * @return An integer array holding all the available years
	 */
	public int[] getYears() {
		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		int[] years = new int[this.rainfallYears.length];

		// Loop through all the years
		for (int i = 0; i < this.rainfallYears.length; i++) {
			// Store each year in an array index position
			years[i] = this.rainfallYears[i].getYear();
		}

		return years;
	}

}
