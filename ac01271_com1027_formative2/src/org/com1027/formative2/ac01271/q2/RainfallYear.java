/**
 * RainfallYear.java
 */

package org.com1027.formative2.ac01271.q2;

/**
 * Defines the attributes & behaviors of the RainfallYear object
 * 
 * @author Alexis Chrysostomou
 */

public class RainfallYear {
	/**
	 * Array of type double used to store all the precipitation of all the months of a
	 * year
	 */
	private double[] rainfallMonths = null;

	/**
	 * Stores the year that will be used to store precipitation data
	 */
	private int year = 0;

	/**
	 * Total amount of months in a year
	 */
	private static final int TOTAL_MONTHS = 12;

	/**
	 * RainfallYear Constructor
	 * 
	 * @param year           The year used to store precipitation data
	 * @param rainfallMonths Monthly precipitation data
	 */
	public RainfallYear(int year, double[] rainfallMonths) {
		super();

		// Additional checks on the parameters
		validateParameters(year, rainfallMonths);

		this.year = year;
		this.rainfallMonths = rainfallMonths;
	}

	/**
	 * @return The year storing precipitation data
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * @param month The enumerated data type of Month
	 * @return The precipitation data of the given Month
	 */
	public double getRainfallMonth(Month month) {
		if(this.rainfallMonths == null)
			throw new NullPointerException("Null rainfallMonths array");
		
		return this.rainfallMonths[month.getMonthId()];
	}

	/**
	 * @return The total monthly precipitation average of all the months of the year
	 */
	public double calculateMeanRainfall() {
		double totalPrecipitacion = 0;

		// Checking if the rainfallMonths array is not null & has the correct size
		if (rainfallMonths == null || rainfallMonths.length != 12)
			throw new NullPointerException("Null/Invalid size rainfallMonths array");

		// Loop through all the months
		for (Month month : Month.values()) {
			totalPrecipitacion += getRainfallMonth(month);
		}

		// Return the average value of all the months
		return (totalPrecipitacion / TOTAL_MONTHS);
	}

	/**
	 * Validates the parameters given in the constructor to ensure there are no
	 * errors
	 * 
	 * @param year           The year given as a parameter in the constructor
	 * @param rainfallMonths The double array given as a parameter in the
	 *                       constructor
	 */
	private void validateParameters(int year, double[] rainfallMonths) {
		// Checking that the year parameter is within boundaries
		if (year < 0 || year > 2019)
			throw new IllegalArgumentException("Invalid year parameter.");

		// Checking if the rainfallMonths array is not null & has the correct size
		if (rainfallMonths == null)
			throw new NullPointerException("Null rainfallMonths array");

		// Checking if rainfallMonths array has the correct size
		if (rainfallMonths.length != 12)
			throw new IllegalArgumentException("rainfallMonths array has invalid size.");
	}
}
