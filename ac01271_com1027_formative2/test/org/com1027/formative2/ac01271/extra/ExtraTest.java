/**
 * ExtraTest.java
 */

package org.com1027.formative2.ac01271.extra;

/**
 * Tests the additional questions classes.
 * 
 * @author Alexis Chrysostomou
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExtraTest {

	/**
	 * Testing the calculation of mean precipitation between two years
	 */
	@Test
	public void testExtraQuestion1() {
		Rainfall rainfall = new Rainfall("rain.txt");

		assertEquals("Incorrect mean precipication", 80.57,
				rainfall.calculateMeanPrecipicationBetweenTwoYears(1914, 1915), 0.1);
	}

	/**
	 * Testing the calculation of the lowest monthly precipitation over all years
	 */
	@Test
	public void testExtraQuestion2() {
		Rainfall rainfall = new Rainfall("rain.txt");

		assertEquals("Incorrect mean precipication", 58.86, rainfall.calculateLowestMonthPrecipitation(), 0.1);
	}

	/**
	 * Testing the histogram generation method
	 */
	@Test
	public void testHistogram() {
		Rainfall rainfall = new Rainfall("rain.txt");

		rainfall.generateHistogram();
	}
}
