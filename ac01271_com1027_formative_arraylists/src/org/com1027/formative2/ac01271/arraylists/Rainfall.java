/**
 * Rainfall.java
 */

package org.com1027.formative2.ac01271.arraylists;

/**
 * Defines the attributes & behaviors of the Rainfall object
 * 
 * @author Alexis Chrysostomou
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rainfall {
	/**
	 * ArrayList of RainfallYear objects used to store data about each year
	 */
	private List<RainfallYear> rainfallYears = null;

	/**
	 * BufferedReader that will be used to read each line of a file
	 */
	private BufferedReader buffer = null;

	/**
	 * FileReader which will be used to read a file
	 */
	private FileReader inputFile = null;

	/**
	 * Regular Expressions that defines the acceptable file format
	 */
	private static final String FILENAME_REGEX = "[a-zA-Z]*.txt";

	/**
	 * Constructor ( Creates an arrayList of RainfallYear objects based on the input
	 * parameter )
	 * 
	 * @param rainfallYear The arrayList holding the rainfall data about each year
	 */
	public Rainfall(ArrayList<RainfallYear> rainfallYears) {
		super();
		this.rainfallYears = rainfallYears;
	}

	/**
	 * Constructor ( Reads a file and creates an arrayList of RainfallYear objects
	 * based of that)
	 * 
	 * @param fileName The name of the file to be read in order to create a
	 *                 RainfallYear object
	 */
	public Rainfall(String fileName) {
		super();
		this.rainfallYears = readFile(fileName);
	}

	/**
	 * Private method that attempts to access and read a file and convert its data
	 * into an arrayList of RainfallYear objects
	 * 
	 * @param fileName The name of the file which will be used to access it
	 * @return An arrayList of RainfallYear objects holding yearly rainfall data
	 */
	public List<RainfallYear> readFile(String fileName) {
		if (!fileName.matches(FILENAME_REGEX))
			throw new IllegalArgumentException("Invalid filename format.");

		try {
			this.inputFile = new FileReader(fileName);
			this.buffer = new BufferedReader(inputFile);

			String line = buffer.readLine();
			ArrayList<Double> rainfallMonths = new ArrayList<Double>();
			int year = 0;
			int previousYear = 0;

			rainfallYears = new ArrayList<RainfallYear>();

			// While the buffer hasn't reached the end of the file
			while (line != null) {
				// Store each word in the arrayList
				List<String> data = Arrays.asList((line.split(",")));

				// Using a try method to ensure that year can be parsed as an Integer
				try {
					// Ensuring that all the years in the file are in sequential order
					if (previousYear + 1 != Integer.parseInt(data.get(0)) && year != 0)
						throw new IllegalArgumentException("Year order is not sequential in the file.");

					// Ensuring that year is not negative
					if (year < 0)
						throw new IllegalArgumentException("Invalid year.");

					year = Integer.parseInt(data.get(0));
					previousYear = year;

				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

				// Collecting rainfallMonths data
				for (int i = 0; i < 12; i++) {
					try {
						// Using a try method to ensure that rainfall month precipitation data can be
						// parsed as double variables
						rainfallMonths.add(Double.parseDouble(data.get(i + 1)));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}

				rainfallYears.add(new RainfallYear(year, rainfallMonths));

				// Resetting the rainfallMonths arrayList to store the new rainfall data of the
				// next
				// line
				rainfallMonths = new ArrayList<Double>();
				line = this.buffer.readLine();
			}

			return rainfallYears;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (this.buffer != null) {
				try {
					// Closing the file
					this.buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	/**
	 * @return The highest average mean precipitation among all available years
	 */
	public double calculateHighestMeanAnnualRainfall() {
		double highestPrecipication = Integer.MIN_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() > highestPrecipication) {
				highestPrecipication = rainfallYear.calculateMeanRainfall();
			}
		}

		return highestPrecipication;
	}

	/**
	 * @return The lowest average mean precipitation among all available years
	 */
	public double calculateLowestMeanAnnualRainfall() {
		double lowestPrecipication = Integer.MAX_VALUE;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.calculateMeanRainfall() < lowestPrecipication) {
				lowestPrecipication = rainfallYear.calculateMeanRainfall();
			}
		}

		return lowestPrecipication;
	}

	/**
	 * 
	 * @param month The enumerated value of month used to get its mean precipitation
	 *              data
	 * @return The mean precipitation data of the given month for every available
	 *         year
	 */
	public double calculateMeanRainfallMonth(Month month) {
		double precipication = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			precipication += rainfallYear.getRainfallMonth(month);
		}

		return (precipication / this.rainfallYears.size());
	}

	/**
	 * 
	 * @param year The year which will be used to get its average precipitation data
	 * @return The average precipitation data of the given year
	 */
	public double calculateMeanRainfallYear(int year) {
		double precipication = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			if (rainfallYear.getYear() == year) {
				precipication = rainfallYear.calculateMeanRainfall();
				break;
			}
		}

		return precipication;
	}

	/**
	 * 
	 * @param year1 The first year holding the rainfall data
	 * @param year2 The second year holding the rainfall data
	 * @return The average mean precipitation between the two given years
	 */
	public double calculateMeanPrecipicationBetweenTwoYears(int year1, int year2) {
		double precipication = 0;

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			// Getting the precipitation of year1
			if (rainfallYear.getYear() == year1) {
				precipication += rainfallYear.calculateMeanRainfall();
			}

			// Getting the precipitation of year2
			if (rainfallYear.getYear() == year2) {
				precipication += rainfallYear.calculateMeanRainfall();
			}
		}

		return (precipication / 2F);
	}

	/**
	 * @return An integer arrayList holding all the available years
	 */
	public List<Integer> getYears() {
		List<Integer> years = new ArrayList<Integer>();

		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("Rainfall Year ArrayList is null.");

		// Loop through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			years.add(rainfallYear.getYear());
		}

		return years;
	}

	/**
	 * @return The lowest precipitation average of a specific month for all the
	 *         years
	 */
	public double calculateLowestMonthPrecipitation() {
		List<Double> monthPrecipitations = Arrays.asList(0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
				0.00, 0.00);

		double lowestPrecipitation = Double.MAX_VALUE;

		// Preventing a null error
		if (rainfallYears == null)
			throw new NullPointerException("Null rainfallYears array");

		// Looping through all the years
		for (RainfallYear rainfallYear : this.rainfallYears) {
			List<Double> yearMonthPrecipitations = rainfallYear.getRainfallMonths();

			// Adding all the month precipitation data of every year into an array
			for (int i = 0; i < 12; i++) {
				monthPrecipitations.set(i, monthPrecipitations.get(i) + yearMonthPrecipitations.get(i));
			}
		}

		// Dividing each month by all rainfallYears to get the precipitation for each
		// month of the year
		for (int i = 0; i < 12; i++) {
			monthPrecipitations.set(i, monthPrecipitations.get(i) / this.rainfallYears.size());
		}

		// Finding the lowest month precipitation
		for (int i = 0; i < 12; i++) {
			if (monthPrecipitations.get(i) < lowestPrecipitation) {
				lowestPrecipitation = monthPrecipitations.get(i);
			}
		}

		return lowestPrecipitation;
	}

	/**
	 * Method that generates a Histogram based on all the precipitation data of all
	 * years
	 */
	public void generateHistogram() {
		// Preventing a null error
		if (this.rainfallYears == null)
			throw new NullPointerException("railYears array is null");

		List<Integer> years = this.getYears();
		//double[] averagePrecipitations = new double[years.length];
		List<Double> averagePrecipitations = new ArrayList<Double>();
		double maxPrecipitation = Double.MIN_VALUE;

		// Header
		System.out.println();
		System.out.println("Average Mean Precipitation Data Histogram");
		System.out.println("Years used: [" + years.get(0) + "-" + years.get(years.size() - 1) + "]");
		System.out.println("Note: Each @@@@ represents 5.0 of the total average precipitation");

		// Collecting average precipitation data & storing them in an array
		for (int i = 0; i < this.rainfallYears.size(); i++) {
			averagePrecipitations.add(this.rainfallYears.get(i).calculateMeanRainfall());
		}

		// Finding the maximum height of the histogram
		for (int i = 0; i < years.size(); i++) {
			if (averagePrecipitations.get(i) > maxPrecipitation) {
				maxPrecipitation = averagePrecipitations.get(i);
			}
		}

		// Printing bars from 0 up to the average precipitation value
		for (double i = maxPrecipitation; i >= 1; i -= 5) {
			for (int k = 0; k < years.size(); k++) {
				if (averagePrecipitations.get(k) >= i) {
					System.out.print("@@@@     ");
				} else {
					System.out.print("         ");
				}
			}
			System.out.println();
		}

		System.out.println();

		DecimalFormat format = new DecimalFormat("00.0");

		// Printing the average precipitation value for each year
		for (int i = 0; i < averagePrecipitations.size(); i++) {
			System.out.print("(" + format.format(averagePrecipitations.get(i)) + ")   ");
		}

		System.out.println();

		// Printing each year
		for (int i = 0; i < averagePrecipitations.size(); i++) {
			System.out.print(years.get(i) + "     ");
		}

	}
}
