/**
 * RainfallYear.java
 */

package org.com1027.formative2.ac01271.arraylists;

/**
 * Defines the attributes & behaviors of the RainfallYear object
 * 
 * @author Alexis Chrysostomou
 */

import java.util.ArrayList;
import java.util.List;

/**
 * RainfallYear class containing all its fields & methods.
 */
public class RainfallYear {
	/**
	 * ArrayList of type double used to store all the precipitation data of all the
	 * months of a year
	 */
	private List<Double> rainfallMonths = null;

	/**
	 * Stores the year that will be used to store precipitation data
	 */
	private int year = 0;

	/**
	 * Total amount of months in a year
	 */
	private static final int TOTAL_MONTHS = 12;

	/**
	 * RainfallYear Constructor
	 * 
	 * @param year           The year used to store precipitation data
	 * @param rainfallMonths Monthly precipitation data
	 */
	public RainfallYear(int year, ArrayList<Double> rainfallMonths) {
		super();

		// Additional checks on the parameters
		validateParameters(year, rainfallMonths);

		this.year = year;
		this.rainfallMonths = rainfallMonths;
	}

	/**
	 * @return The year storing precipitation data
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * @param month The enumerated data type of Month
	 * @return The precipitation data of the given Month
	 */
	public double getRainfallMonth(Month month) {
		if (this.rainfallMonths == null)
			throw new NullPointerException("Null rainfallMonths arrayList");

		return this.rainfallMonths.get(month.getMonthId());
	}

	/**
	 * @return A double List of all the rainfall data for each month
	 */
	public List<Double> getRainfallMonths() {
		if (this.rainfallMonths == null)
			throw new NullPointerException("Null rainfallMonths array");

		return this.rainfallMonths;
	}

	/**
	 * @return The total monthly precipitation average of all the months of the year
	 */
	public double calculateMeanRainfall() {
		double totalPrecipitation = 0;

		// Checking if the rainfallMonths arrayList is not null & has the correct size
		if (this.rainfallMonths == null || this.rainfallMonths.size() != 12)
			throw new NullPointerException("Null/Invalid size rainfallMonths array");

		// Loop through all the months
		for (Month month : Month.values()) {
			totalPrecipitation += getRainfallMonth(month);
		}

		// Return the average value of all the months
		return (totalPrecipitation / TOTAL_MONTHS);
	}

	/**
	 * Validates the parameters given in the constructor to ensure there are no
	 * errors
	 * 
	 * @param year           The year given as a parameter in the constructor
	 * @param rainfallMonths The double array given as a parameter in the
	 *                       constructor
	 */
	private void validateParameters(int year, ArrayList<Double> rainfallMonths) {
		// Checking that the year parameter is within boundaries
		if (year < 0 || year > 2019)
			throw new IllegalArgumentException("Invalid year parameter.");

		// Checking if the rainfallMonths arrayList is not null & has the correct size
		if (rainfallMonths == null)
			throw new NullPointerException("Null rainfallMonths array");

		// Checking if rainfallMonths arrayList has the correct size
		if (rainfallMonths.size() != 12)
			throw new IllegalArgumentException("rainfallMonths array has invalid size.");
	}
}
