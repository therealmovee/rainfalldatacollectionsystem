/**
 * AllTests.java
 */

package org.com1027.formative2.ac01271.arraylists;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * This class defines the JUnit4 test suite for the rainfall coursework.
 * 
 * @author Matthew Casey
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ RainfallTest.class, RainfallYearTest.class, ExtraTest.class })
public class AllTests {
}
