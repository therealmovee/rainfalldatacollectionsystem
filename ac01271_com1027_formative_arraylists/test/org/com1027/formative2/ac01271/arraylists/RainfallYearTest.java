/**
 * RainfallTest.java
 */

package org.com1027.formative2.ac01271.arraylists;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.com1027.formative2.ac01271.arraylists.Month;
import org.com1027.formative2.ac01271.arraylists.RainfallYear;
import org.junit.Test;

/**
 * Tests for the <code>RainfallYear</code> class.
 * 
 * @author Matthew Casey
 */
public class RainfallYearTest {

	/**
	 * Test object construction.
	 */
	@Test
	public void testConstruction() {
		ArrayList<Double> rainfallMonths = new ArrayList<Double>();

		for (int i = 0; i <= 11; i++) {
			rainfallMonths.add((double) i);
		}

		// Create an object with a line to parse.
		RainfallYear rainfallYear = new RainfallYear(2009, rainfallMonths);

		// Test that the correct values have been extracted.
		assertEquals("Incorrect year", 2009, rainfallYear.getYear());

		for (int i = 0; i < 12; i++) {
			assertEquals("Incorrect rainfall", i, rainfallYear.getRainfallMonth(Month.values()[i]), 0);
			System.out
					.println(Month.values()[i] + ": " + i + " == " + rainfallYear.getRainfallMonth(Month.values()[i]));
		}
	}

	/**
	 * Test calculation of the mean rainfall.
	 */
	@Test
	public void testMeanRainfall() {
		ArrayList<Double> rainfallMonths = new ArrayList<Double>();

		for (int i = 0; i <= 11; i++) {
			rainfallMonths.add((double) i);
		}

		// Create an object with a line to parse.
		RainfallYear rainfallYear = new RainfallYear(2009, rainfallMonths);

		// Test that the mean is correct.
		double expectedMean = (0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11) / (double) 12;
		assertEquals("Incorrect mean", expectedMean, rainfallYear.calculateMeanRainfall(), 0);
	}
}
